import React, { Component } from 'react'
import './json-viewer.css'

export default class JsonViewer extends Component {
  constructor (props) {
    super(props)
    this.state = { showBlock: false }
  }

  toggleBlock = () => {
    this.setState((prevState) => ({
      showBlock: !prevState.showBlock,
    }))
  }

  render () {
    if (this.state.showBlock) {
      return (
        <div className="json-wrapper">
          <div className="button-wrapper">
            <button className="default"
                    onClick={this.toggleBlock}>Скрыть JSON
            </button>
          </div>
          <div className="json-item">
            {JSON.stringify(this.props.data)}
          </div>
        </div>
      )
    }

    return (
      <div className="json-wrapper">
        <div className="button-wrapper">
          <button className="default"
                  onClick={this.toggleBlock}>Показать JSON
          </button>
        </div>
      </div>
    )
  }
}
