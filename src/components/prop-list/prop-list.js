import React, {Component} from 'react';
import './prop-list.css';
import PropChangeForm from '../prop-change-form';

export default class PropList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showBlock: false,
      activeProp: null,
    };
  }

  onPropsDelete = (e) => {
    let id;
    if (e.currentTarget.dataset.id === 0 || e.currentTarget.dataset.id) {
      id = e.currentTarget.dataset.id;
    }
    if (id) {
      this.props.onPropsDelete(id);
    }
  };

  setActiveProp = (e) => {
    let activeProp = this.state.activeProp;
    if (e.currentTarget.dataset.id === 0 || e.currentTarget.dataset.id) {
      activeProp = e.currentTarget.dataset.id;
    }
    this.setState({
      activeProp,
    });
  };

  onSaveClick = () => {
    this.setState(
        {activeProp: null});
  };

  toggleBlock = () => {
    this.setState((prevState) => ({
      showBlock: !prevState.showBlock,
    }));
  };

  onSubmit = (e) => {
    e.preventDefault();
    const {propName, propValue} = this.state;
    this.setState({propName: '', propValue: ''});
    this.props.onPropsAdd(propName, propValue);
  };

  render() {
    const ActiveProps = (props) => {
      const {attr} = props;
      const {activeProp} = this.state;
      if (!attr) {
        return ('');
      }

      const listItems = attr.map((item, idx) => {
            if (activeProp == idx) {
              return (
                  <li key={idx}>
                    <PropChangeForm
                        name={item.propName}
                        value={item.propValue}
                        id={idx}
                        onPropsAdd={this.props.onPropsAdd}
                        onSaveClick={this.onSaveClick}
                        onPropsDelete={this.props.onPropsDelete}
                    />
                  </li>
              );
            }

            return (
                <li key={idx}><span className="name">{item.propName}</span><span
                    className="value">{item.propValue}</span>
                  <button data-id={idx} className="default"
                          onClick={this.setActiveProp}>Изменить
                  </button>
                  <button data-id={idx} className="default"
                          onClick={this.onPropsDelete}>Удалить
                  </button>
                </li>
            );
          },
      );
      return (
          <ul className="props">{listItems}</ul>
      );
    };

    if (Object.getOwnPropertyNames(this.props.activeItem).length === 0) {
      return '';
    }

    if (!this.state.showBlock) {
      return (
          <div>
            <button className="default"
                    onClick={this.toggleBlock}>Посмотреть/изменить свойства
            </button>
          </div>
      );
    }

    return (
        <div>
          <button className="default"
                  onClick={this.toggleBlock}>Посмотреть/изменить свойства
          </button>

          <ActiveProps attr={this.props.activeItem.attributes}/>

          <PropChangeForm
              name=''
              value=''
              onPropsAdd={this.props.onPropsAdd}
          />
        </div>
    );
  }
}
