import React, {Component} from 'react';
import './child-change-form.css';

export default class ChildChangeForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: this.props.name,
    };
  }

  handleFormChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  };

  onChildDelete = (id) => {
    this.props.onChildDelete(id);
  };

  saveChild = (e) => {
    e.preventDefault();
    const {name} = this.state;
    this.setState({name: ''});
    this.props.onChildSaveClick();
    this.props.onChildAdd(name, this.props.id);
  };

  addChild = (e) => {
    e.preventDefault();
    const {name} = this.state;
    this.setState({name: ''});
    this.props.onChildAdd(name, this.props.id);
  };

  render() {
    if (this.props.id === 0 || this.props.id) {
      return (
          <form className="child-change" onSubmit={this.onSubmit}>
            <input type="text"
                   className=""
                   name="name"
                   value={this.state.name}
                   placeholder="Название потомка"
                   onChange={this.handleFormChange}
            />
            <button className="default"
                    type="submit"
                    onClick={this.saveChild}>Сохранить
            </button>
            <button
                className='default'
                onClick={this.onChildDelete}> Удалить
            </button>
          </form>
      );
    }

    return (
        <form className="child-change" onSubmit={this.onSubmit}>
          <input type="text"
                 className=""
                 name="name"
                 value={this.state.name}
                 placeholder="Название потомка"
                 onChange={this.handleFormChange}
          />
          <button className="default"
                  type="submit"
                  onClick={this.addChild}>Добавить
          </button>
        </form>
    );
  }
}

