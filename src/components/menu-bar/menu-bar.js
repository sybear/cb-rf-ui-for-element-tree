import React, { Component } from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Drawer from '@material-ui/core/Drawer'
import { withStyles } from '@material-ui/core/styles'
import './menu-bar.css'

const styles = {
  list: {
    width: 300,
  },
  links: {
    textDecoration:'none',
  },
  menuHeader: {
    paddingLeft: '30px'
  }
};
class MenuBar extends Component {
  constructor( props ) {
    super( props )
    this.state = {}
  }

  handleClick( item ) {
    this.setState( prevState => (
        { [ item ]: !prevState[ item ] }
    ) )
  }



  handler( items ) {
    const { state } = this

    return items.map( ( item ) => {
      let activeClass = '';
      if (item.id === this.props.activeItem.id) {
        activeClass = 'active';
      }

      if ( !item.children || item.children.length === 0 ) {
        return (
            <li key={ item.id } className={activeClass}>
              <ListItem
                  button
                  >

                  <ListItemText

                      inset
                      primary={ item.name }
                      onClick={ () => this.props.setActiveItem( item ) }
                  />

              </ListItem>
            </li>
        )
      }
      return (
          <li key={ item.id } className={activeClass}>
            <ListItem
                button
                >
              <ListItemText
                  inset
                  primary={ item.name }

                  onClick={ () => this.props.setActiveItem( item ) }
              />
              { state[ item.id ] ?
                  <ExpandLess
                      onClick={ () => this.handleClick( item.id ) }
                  /> :
                  <ExpandMore
                      onClick={ () => this.handleClick( item.id ) }
                  />
              }
            </ListItem>
            <Collapse
                in={ state[ item.id ] }
                timeout="auto"
                unmountOnExit
            >
              <ul>
              { this.handler( item.children ) }
              </ul>
            </Collapse>
          </li>
      )
    } )
  }

  render() {
    const { classes, menuItems } = this.props

    return (
        <div className={classes.list}>
          <Drawer
              variant="permanent"
              anchor="left"
              open
              classes={ { paper: classes.list } }>
            <div>
              <List>
                <ListItem
                    key="menuHeading"
                    divider
                    disableGutters
                >
                  <ListItemText
                      className={ classes.menuHeader }
                      inset
                      primary="Дерево элементов"
                      onClick={this.props.clearActiveItem}
                  />
                </ListItem>
                { this.handler( menuItems ) }
              </List>
            </div>
          </Drawer>
        </div>
    )
  }
}
export default withStyles(styles)(MenuBar)