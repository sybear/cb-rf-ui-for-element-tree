import React, { Component } from 'react'
import './child-list.css'
import ChildChangeForm from '../child-change-form'

export default class ChildList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showBlock: false,
      activeChild: null,
      name: '',
    }
  }

  onChildDelete = (e) => {
    let id
    if (e.currentTarget.dataset.id === 0 || e.currentTarget.dataset.id) {
      id = e.currentTarget.dataset.id
    }
    if (id) {
      this.props.onChildDelete(id)
    }
  }

  setActiveChild = (e) => {
    let activeChild = this.state.activeChild
    if (e.currentTarget.dataset.id === 0 || e.currentTarget.dataset.id) {
      activeChild = e.currentTarget.dataset.id
    }
    this.setState({
      activeChild,
    })
  }

  onChildSaveClick = () => {
    this.setState(
      { activeChild: null })
  }

  toggleBlock = () => {
    this.setState((prevState) => ({
      showBlock: !prevState.showBlock,
    }))
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { name } = this.state
    this.setState({ name: '' })
    this.props.onChildAdd(name)
  }

  render () {
    const ChildrensList = (props) => {
      const { items } = props
      const { activeChild } = this.state
      if (!items) {
        return ('')
      }

      const listItems = items.map((item, idx) => {
          if (activeChild == item.id) {
            return (
              <li key={idx}>
                <ChildChangeForm
                  name={item.name}
                  id={idx}
                  onChildAdd={this.props.onChildAdd}
                  onChildSaveClick={this.onChildSaveClick}
                  onChildDelete={this.props.onChildDelete}
                />
              </li>
            )
          }

          return (
            <li key={item.id}>
              <span className="name">{item.name}</span>
              <button data-id={item.id} className="default"
                      onClick={this.setActiveChild}>Изменить
              </button>
              <button data-id={item.id} className="default"
                      onClick={this.onChildDelete}>Удалить
              </button>
            </li>
          )
        },
      )
      return (
        <ul className="props">{listItems}</ul>
      )
    }

    if (!this.state.showBlock) {
      return (
        <div>
          <button className="default"
                  onClick={this.toggleBlock}>Посмотреть/изменить потомков
          </button>
        </div>
      )
    }

    return (
      <div>
        <button className="default"
                onClick={this.toggleBlock}>Посмотреть/изменить потомков
        </button>

        <ChildrensList items={this.props.activeItem.children}/>

        <ChildChangeForm
          name=''
          onChildAdd={this.props.onChildAdd}
        />
      </div>
    )
  }
}