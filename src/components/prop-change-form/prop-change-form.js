import React, {Component} from 'react';
import './prop-change-form.css';

export default class PropChangeForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      propName: this.props.name,
      propValue: this.props.value,
    };
  }

  handleFormChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  };

  onPropsDelete = (id) => {
    this.props.onPropsDelete(id);
  };

  saveProp = (e) => {
    e.preventDefault();
    const {propName, propValue} = this.state;
    this.setState({propName: '', propValue: ''});
    this.props.onSaveClick();
    this.props.onPropsAdd(propName, propValue, this.props.id);
  };

  addProp = (e) => {
    e.preventDefault();
    const {propName, propValue} = this.state;
    this.setState({propName: '', propValue: ''});
    this.props.onPropsAdd(propName, propValue, this.props.id);
  };

  render() {
    if (this.props.id === 0 || this.props.id) {
      return (
          <form className="props-change" onSubmit={this.onSubmit}>
            <input type="text"
                   className=""
                   name="propName"
                   value={this.state.propName}
                   placeholder="Название свойства"
                   onChange={this.handleFormChange}
            />
            <input
                type='text'
                name='propValue'
                className=''
                value={this.state.propValue}
                placeholder='Значение свойства'
                onChange={this.handleFormChange}
            />
            <button className="default"
                    type="submit"
                    onClick={this.saveProp}>Сохранить
            </button>
            <button
                className='default'
                onClick={this.onPropsDelete}> Удалить
            </button>
          </form>
      );
    }

    return (
        <form className="props-change" onSubmit={this.onSubmit}>
          <input type="text"
                 className=""
                 name="propName"
                 value={this.state.propName}
                 placeholder="Название свойства"
                 onChange={this.handleFormChange}
          />
          <input
              type='text'
              name='propValue'
              className=''
              value={this.state.propValue}
              placeholder='Значение свойства'
              onChange={this.handleFormChange}
          />
          <button className="default"
                  type="submit"
                  onClick={this.addProp}>Добавить
          </button>
        </form>
    );
  }
}

