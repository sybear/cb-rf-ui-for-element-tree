import res from '../data/menuItems';

export default class MenuItemsService {

  _data = res.data;

  getResource = () => {
    return this._data;
  };

  getMaxId = () => {
    let max = 0;
    max = this.searchMax(this._data, max);
    return max+1;
  };

  searchMax = (arr, max) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id >= max) {
        max = arr[i].id;
      }
      if (arr[i].hasOwnProperty('children')) {
        const maxChild = this.searchMax(arr[i].children, max)
        if (maxChild > max) {
          max = maxChild;
        }
      }
    }
    return max;
  }

}
