import React, { Component } from 'react'
import MenuBar from './components/menu-bar/menu-bar'
import PropList from './components/prop-list'
import ChildList from './components/child-list'
import MenuItemsService from './services/menu-items-service'
import JsonViewer from './components/json-viewer'
import './App.css'

class App extends Component {
  menuItemsService = new MenuItemsService()

  constructor (props) {
    super(props)
    this.state = {
      activeItem: {},
      idCounter: this.menuItemsService.getMaxId(),
      menuItems: this.menuItemsService.getResource(),
    }
  }

  setActiveItem = (activeItem) => {
    this.setState({
      activeItem,
    })
  }

  clearActiveItem = () => {
    this.setState({
      activeItem: {},
    })
  }

  onChildDelete = (id) => {

    this.setState((prevState) => {
      const menuItems = this.deleteChild(prevState, id)
      return { menuItems }
    })
  }

  deleteChild = (prevState, id) => {
    const { menuItems } = prevState
    const menuItemsDublicate = [...menuItems]

    this.deleteObj(menuItemsDublicate, id)
    console.log(menuItemsDublicate)
    return menuItemsDublicate

  }

  deleteObj = (arr, id) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id == id) {
        arr.splice(i, 1)
        return true
      }
      if (arr[i].hasOwnProperty('children')) {
        const res = this.deleteObj(arr[i].children, id)
        if (res) {
          return true
        }
      }
    }
    return false
  }

  onPropsDelete = (id) => {
    this.setState((prevState) => {
      const menuItems = this.deleteProp(prevState, id)
      return { menuItems }
    })
  }

  deleteProp = (prevState, id) => {
    const { menuItems, activeItem } = prevState
    const menuItemsDublicate = [...menuItems]

    const activeItemDublicate = this.findObj(menuItemsDublicate, activeItem.id)

    if (activeItemDublicate) {
      if (id === 0 || id) {
        activeItemDublicate.attributes.splice(id, 1)
      }
    }
    return menuItemsDublicate
  }

  onChildAdd = (name, id) => {
    this.setState((prevState) => {
      const menuItems = this.addChild(prevState, name, id)
      const idCounter = prevState.idCounter + 1
      return { menuItems, idCounter }
    })
  }

  addChild = (prevState, name, id) => {
    const { menuItems, activeItem, idCounter } = prevState
    const menuItemsDuplicate = [...menuItems]

    if (Object.getOwnPropertyNames(activeItem).length === 0) {
      menuItemsDuplicate.push({ id: idCounter, name })
    } else {
      const activeItemDuplicate = this.findObj(menuItemsDuplicate,
        activeItem.id)

      if (activeItemDuplicate) {
        if (id === 0 || id) {
          activeItemDuplicate.children[id].name = name
        } else {
          if (!activeItemDuplicate.hasOwnProperty('children')) {
            activeItemDuplicate.children = []
          }
          activeItemDuplicate.children.push(
            {
              id: idCounter,
              name,

            })
        }
      }
    }
    return menuItemsDuplicate
  }

  onPropsAdd = (propName, propValue, id) => {
    this.setState((prevState) => {
      const menuItems = this.addProp(prevState, propName, propValue, id)
      return { menuItems }
    })
  }

  addProp = (prevState, propName, propValue, id) => {

    const { menuItems, activeItem } = prevState
    const menuItemsDublicate = [...menuItems]

    const activeItemDublicate = this.findObj(menuItemsDublicate, activeItem.id)

    if (activeItemDublicate) {
      if (id === 0 || id) {
        activeItemDublicate.attributes[id].propName = propName
        activeItemDublicate.attributes[id].propValue = propValue
      } else {
        if (!activeItemDublicate.hasOwnProperty('attributes')) {
          activeItemDublicate.attributes = []
        }
        activeItemDublicate.attributes.push(
          { propName: propName, propValue: propValue })
      }
    }
    return menuItemsDublicate
  }

  findObj = (arr, id) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id == id) {
        return arr[i]
      }
      if (arr[i].hasOwnProperty('children')) {
        const obj = this.findObj(arr[i].children, id)
        if (obj) {
          return obj
        }
      }
    }
    return false
  }

  render () {
    return (
      <div className="file-interface-app">
        <MenuBar
          setActiveItem={this.setActiveItem}
          clearActiveItem={this.clearActiveItem}
          activeItem={this.state.activeItem}
          menuItems={this.state.menuItems}
        />


        <div className="main-content">
          <div className="item">

            <PropList onPropsAdd={this.onPropsAdd}
                      onPropsDelete={this.onPropsDelete}
                      activeItem={this.state.activeItem}/>

            <ChildList onChildAdd={this.onChildAdd}
                       activeItem={this.state.activeItem}
                       onChildDelete={this.onChildDelete}

            />
          </div>
          <JsonViewer data={this.state.menuItems}/>
        </div>
      </div>

    )
  }
}

export default App